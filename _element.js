import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `borrador-proyecto-front`
 * 
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class BorradorProyectoFront extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <h2>Hello [[prop1]]!</h2>
    `;
  }
  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'borrador-proyecto-front',
      },
    };
  }
}

window.customElements.define('borrador-proyecto-front', BorradorProyectoFront);
